```
.
├── readme.md
├── Crypto
│   ├── applied_cryptography_protocols_algorithms_and_source_code_in_c.pdf
│   ├── cryptography_engineering_design_principles_and_practical_applications.pdf
│   └── investigatingcryptocurrencies.pdf
├── Debug Life
│   ├── askoutrageously.pdf
│   ├── changeyourquestionschangeyourlife_ebook.pdf
│   ├── collaboratingwiththeenemy_ebook.pdf
│   ├── convinced_ebook.pdf
│   ├── crunchtime_ebook.pdf
│   ├── frommindfulnesstoheartfulness_ebook.pdf
│   ├── gettothepoint_ebook.pdf
│   ├── negotiatingtheimpossible.pdf
│   ├── prisonersofourthoughts_ebook.pdf
│   ├── stopguessing.pdf
│   ├── theageofoverwhelm_ebook.pdf
│   ├── thegiftofanger.pdf
│   ├── themoodelevator_ebook.pdf
│   ├── theoutwardmindset_ebook.pdf
│   ├── thepowerofhavingfun_ebook.pdf
│   ├── theseriousbusinessofsmalltalk_ebook.pdf
│   ├── theshift_ebook.pdf
│   ├── whypeopledontbelieveyou_ebook.pdf
│   ├── youarewhatyoubelieve_ebook.pdf
│   └── youngmoney_ebook.pdf
├── Electronics
│   ├── gettingstartedwithnetduino1.pdf
│   ├── gettingstartedwiththeinternetofthings1.pdf
│   ├── make_basicarduinoprojects.pdf
│   ├── make_gettingstartedwithadafruitflora.pdf
│   ├── make_gettingstartedwithadafruittrinket.pdf
│   ├── make_magazine_38_1.pdf
│   └── makingthingssee.pdf
├── Linux
│   ├── assemblylanguagestep-by-step_programmingwithlinux.pdf
│   ├── beginninglinuxprogramming.pdf
│   ├── linuxall-in-onefordummies.pdf
│   ├── linuxbible.pdf
│   ├── linuxcommandline_shellscriptingbible.pdf
│   ├── linuxessentials.pdf
│   ├── linuxserversecurity_hackanddefend.pdf
│   ├── lpic-1_linuxprofessionalinstitutecertificationstudyguide_exams101_102.pdf
│   ├── lpic-2_linuxprofessionalinstitutecertificationstudyguide_exams201_202.pdf
│   ├── professionallinuxkernelarchitecture.pdf
│   ├── redhatenterpriselinux6administration.pdf
│   ├── shellcoders_handbook_discovering_and_exploiting_security_holes.pdf
│   ├── shellscripting_expertrecipesforlinuxbashandmore.pdf
│   └── ubuntulinuxtoolbox_1000pluscommandsforpowerusers.pdf
├── Orielly Software
│   ├── 97thingseveryprogrammershouldknow.pdf
│   ├── algorithmsinanutshell.pdf
│   ├── apprenticeshippatterns.pdf
│   ├── artofreadablecode.pdf
│   ├── becomingabetterprogrammer.pdf
│   ├── headfirstagile_ebook.pdf
│   ├── introductiontomachinelearningwithpython.pdf
│   ├── regularexpressionscookbook.pdf
│   ├── technologystrategypatterns.pdf
│   └── testdrivendevelopmentwithpython.pdf
├── PacktProgramming
│   ├── cleancodeinpython.pdf
│   ├── cplusplushighperformance.pdf
│   ├── csharp71anddotnetcore20_moderncross-platformdevelopment.pdf
│   ├── csharp7anddotnetcorecookbook.pdf
│   ├── expertpythonprogramming.pdf
│   ├── functionalpythonprogramming.pdf
│   ├── gocookbook.pdf
│   ├── java11cookbook.pdf
│   ├── learningjavabybuildingandroidgames.pdf
│   ├── learningjavascriptdatastructuresandalgorithms.pdf
│   ├── learnjava12programming.pdf
│   ├── masteringgo.pdf
│   ├── moderncplusplusprogrammingcookbook.pdf
│   ├── python3objectorientedprogramming.pdf
│   ├── pythonprogrammingblueprints.pdf
│   ├── softwarearchitectshandbook.pdf
│   ├── themoderncpluspluschallenge.pdf
│   └── understandingsoftware.pdf
├── Penetration Testing
│   ├── advancedpenetrationtesting.pdf
│   ├── art_of_deception_controlling_the_human_element_of_security.pdf
│   ├── art_of_memory_forensics_detecting_malware_and_threats_in_windows_linux_and_mac_memory.pdf
│   ├── ceh_v9_certified_ethical_hacker_study_guide.pdf
│   ├── comptialinuxplus_lpicpracticetests_examslx0-103_lpic-1101-400_lx0-104_lpic-1102.pdf
│   ├── comptialinuxpluspoweredbylinuxprofessionalinstitutestudyguide_examslx0-103_lx0-104.pdf
│   ├── malware_analysts_cookbook_and_dvd_tools_and_techniques_for_fighting_malicious_code.pdf
│   ├── practical_reverse_engineering_x86_x64_arm_windows_kernel_reversing_tools_and_obfuscation.pdf
│   ├── reversing_secretsofreverseengineering.pdf
│   ├── secrets_and_lies_digital_security_in_a_networked_world.pdf
│   ├── security_engineering_a_guide_to_building_dependable_distributed_systems.pdf
│   ├── social_engineering_the_art_of_human_hacking.pdf
│   ├── threat_modeling_designing_for_security.pdf
│   ├── unauthorised_access_physical_penetration_testing_for_it_security_teams.pdf
│   └── wiresharkforsecurityprofessionals.pdf
├── Python
│   ├── automatetheboringstuffwithpython_2e.pdf
│   ├── blackhatpython.pdf
│   ├── crackingcodeswithpython.pdf
│   ├── grayhatpython.pdf
│   ├── impracticalpythonprojects.pdf
│   ├── inventyourowncomputergameswithpython.pdf
│   ├── mathadventureswithpython.pdf
│   ├── missionpython.pdf
│   ├── pythoncrashcourse2ndedition.pdf
│   ├── pythonflashcards.pdf
│   ├── pythonforkids_1490905401.pdf
│   ├── pythonplayground_geekyprojectsforthecuriousprogrammer.pdf
│   ├── seriouspython.pdf
│   └── teachyourkidstocode_1490913058.pdf
└── Web Programming
    ├── angular_upandrunning.pdf
    ├── csspocketreference.pdf
    ├── css_thedefinitiveguide.pdf
    ├── datavisualizationwithpythonandjavascript.pdf
    ├── designingwebapis.pdf
    ├── flaskwebdevelopment.pdf
    ├── highperformanceimages.pdf
    ├── learninggraphql.pdf
    ├── learningjavascript.pdf
    ├── learningphpmysqlandjavascript.pdf
    ├── learningreact1.pdf
    ├── masteringmodularjavascript.pdf
    ├── usingsvgwithcss3andhtml5.pdf
    ├── vuejs_upandrunning.pdf
    ├── web_application_hackers_handbook_finding_and_exploiting_security_flaws.pdf
    ├── youdontknowjs_es6andbeyond.pdf
    ├── youdontknowjs_scopeandclosures.pdf
    └── youdontknowjs_upandgoing.pdf

9 directories, 120 files
```
